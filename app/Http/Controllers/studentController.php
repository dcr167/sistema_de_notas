<?php

namespace App\Http\Controllers;
use App\Model\student;
use Illuminate\Http\Request;

class studentController extends Controller
{
    public function show(){
        return student::all();

    }
    public function destroy($id){
        $student = Student::find($id);
        $student->delete();
        return 'deleted';
    }
    public function store(Request $request){
        $student = new Student();
        $student->name_student = $request->name_student;
        $student->lastname_student = $request->lastname_student;
        $student->ci_student = $request->ci_student;
        $student->age_student = $request->age_student;
        $student->cellphone_student = $request->cellphone_student;
        $student->email_student = $request->email_student;
        $student->save();
        return 'saved';
    }



}

<?php

namespace App\Http\Controllers;
use App\Model\teachers;
use Illuminate\Http\Request;

class teachersController extends Controller
{
    public function show(){
        return teachers::all();

    }
    public function destroy($id){
        $teachers = Teachers::find($id);
        $teachers->delete();
        return 'deleted';
    }
    public function store(Request $request){
        $teachers = new Teachers;
        $teachers->name_teacher = $request->name_teacher;
        $teachers->lastname_teacher = $request->lastname_teacher;
        $teachers->subject_teacher = $request->subject_teacher;
        $teachers->ci_teacher = $request->ci_teacher;
        $teachers->age_teacher = $request->age_teacher;
        $teachers->cellphone_teacher = $request->cellphone_teacher;
        $teachers->email_teacher = $request->email_teacher;
        $teachers->save();
        return 'saved';
    }
}

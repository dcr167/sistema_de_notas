<?php

namespace App\Http\Controllers;
use App\Model\notes;
use Illuminate\Http\Request;

class notesController extends Controller
{
    public function show(){
        return notes::all();

    }
    public function destroy($id){
        $notes = Notes::find($id);
        $notes->delete();
        return 'deleted';
    }
    public function store(Request $request){
        $notes = new Notes();
        $notes->subject_note = $request->subject_note;
        $notes->first_partial = $request->first_partial;
        $notes->second_partial = $request->second_partial;
        $notes->final_partial = $request->final_partial;
        $notes->save();
        return 'saved';
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class notes extends Model
{
    protected $table= 'notes';
    protected $primarykey= 'id_note';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'subject_note',
        'first_partial',
        'second_partial',
        'final_partial',
        'date_create',
        'date_update',
    ];
}

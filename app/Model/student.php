<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table= 'student';
    protected $primarykey= 'id_student';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'name_student',
        'lastname_student',
        'ci_student',
        'age_student',
        'cellphone_student',
        'email_student',
        'date_create',
        'date_update',
    ];
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class teachers extends Model
{
    protected $table= 'teachers';
    protected $primarykey= 'id_teacher';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'name_teacher',
        'lastname_teacher',
        'subject_teacher',
        'ci_teacher',
        'age_teacher',
        'cellphone_teacher',
        'email_teacher',
        'date_create',
        'date_update',
    ];
}

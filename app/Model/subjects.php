<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class subjects extends Model
{
    protected $table= 'subjects';
    protected $primarykey= 'id_subject';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'name_subject',
        'schedule_subject',
        'teacher_subject',
        'name_group',
        'date_create',
        'date_update',
    ];
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id_student');
            $table->string('name_student')->nullable();
            $table->string('lastname_student')->nullable();
            $table->integer('ci_student')->nullable();
            $table->integer('age_student')->nullable();
            $table->integer('cellphone_student')->nullable();
            $table->string('email_student')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->dateTime('date_update')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}

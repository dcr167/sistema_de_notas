<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id_teacher');
            $table->string('name_teacher')->nullable();
            $table->string('lastname_teacher')->nullable();
            $table->string('subject_teacher')->nullable();
            $table->integer('ci_teacher')->nullable();
            $table->integer('age_teacher')->nullable();
            $table->integer('cellphone_teacher')->nullable();
            $table->string('email_teacher')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->dateTime('date_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}

-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2019 a las 05:34:34
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `system_notes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_05_233325_create_student_table', 1),
(2, '2019_06_05_233923_create_subjects_table', 1),
(3, '2019_06_05_234405_create_teachers_table', 1),
(4, '2019_06_05_234815_create_notes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes`
--

CREATE TABLE `notes` (
  `id_note` bigint(20) UNSIGNED NOT NULL,
  `subject_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_partial` int(11) DEFAULT NULL,
  `second_partial` int(11) DEFAULT NULL,
  `final_partial` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notes`
--

INSERT INTO `notes` (`id_note`, `subject_note`, `first_partial`, `second_partial`, `final_partial`, `date_create`, `date_update`) VALUES
(1, 'Matematicas', 58, 60, 51, '2019-06-20 00:00:00', '2019-06-26 00:00:00'),
(2, 'Calculo I', 50, 48, 70, '2019-06-20 00:00:00', '2019-06-27 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `id_student` bigint(20) UNSIGNED NOT NULL,
  `name_student` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname_student` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_student` int(11) DEFAULT NULL,
  `age_student` int(11) DEFAULT NULL,
  `cellphone_student` int(11) DEFAULT NULL,
  `email_student` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`id_student`, `name_student`, `lastname_student`, `ci_student`, `age_student`, `cellphone_student`, `email_student`, `date_create`, `date_update`) VALUES
(1, 'Dante', 'Elrick', 9568454, 20, 78489489, 'dan777@89gmail.com', '2019-06-26 00:00:00', '2019-06-27 00:00:00'),
(2, 'Nero', 'Cabrera', 589649, 25, 59649878, 'nero666@gmail.com', '2019-06-27 00:00:00', '2019-06-27 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subjects`
--

CREATE TABLE `subjects` (
  `id_subject` bigint(20) UNSIGNED NOT NULL,
  `name_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `subjects`
--

INSERT INTO `subjects` (`id_subject`, `name_subject`, `schedule_subject`, `teacher_subject`, `name_group`, `date_create`, `date_update`) VALUES
(1, 'Matematicas', '9:00-10:30', 'Luisito', 'A', '2019-06-20 00:00:00', '2019-06-26 00:00:00'),
(2, 'Calculo I', '14:00-15:30', 'Issac Clark', 'B', '2019-06-27 00:00:00', '2019-06-28 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE `teachers` (
  `id_teacher` bigint(20) UNSIGNED NOT NULL,
  `name_teacher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname_teacher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_teacher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_teacher` int(11) DEFAULT NULL,
  `age_teacher` int(11) DEFAULT NULL,
  `cellphone_teacher` int(11) DEFAULT NULL,
  `email_teacher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `teachers`
--

INSERT INTO `teachers` (`id_teacher`, `name_teacher`, `lastname_teacher`, `subject_teacher`, `ci_teacher`, `age_teacher`, `cellphone_teacher`, `email_teacher`, `date_create`, `date_update`) VALUES
(1, 'Markus', 'Finix', 'Matematicas', 4564894, 38, 7894848, 'mark789@finix@gmail.com', '2019-06-21 00:00:00', '2019-06-28 00:00:00'),
(2, 'Vash', 'Stampida', 'Calculo I', 789465984, 45, 458878, 'vash78@gamil.com', '2019-06-28 00:00:00', '2019-06-28 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id_note`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_student`);

--
-- Indices de la tabla `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id_subject`);

--
-- Indices de la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id_teacher`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `notes`
--
ALTER TABLE `notes`
  MODIFY `id_note` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `id_student` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id_subject` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id_teacher` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
